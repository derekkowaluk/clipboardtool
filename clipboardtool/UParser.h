#pragma once

#include <string>
#include <vector>
#include <stdint.h>
#include <unordered_map>


using result_type = int;

inline bool is_quote(char c)
{
	return (c == '\'') || (c == '\"');
}


inline bool is_alpha(char c)
{
	if (c == '_') return true;
	return ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'));
}

inline bool is_numeric(char c)
{
	return (c >= '0') && (c <= '9');
}

inline bool is_numeric_or_decimal(char c)
{
	return is_numeric(c) || (c == '.');
}

inline bool is_alpha_or_numeric(char c)
{
	return is_alpha(c) || is_numeric(c);
}

inline bool is_alpha_or_numeric_or_quotes(char c)
{
	return is_alpha(c) || is_numeric(c) || is_quote(c);
}


using tokenlist = std::unordered_map<u32, std::string>;
using string_iter = std::string::const_iterator;

enum class parse_type
{
	structure,
	token,
	literal,
	value
};


struct parse_element
{
	parse_type	type;
	u32	index;
	string_iter start;
	string_iter end;
};

using parse_element_list = std::vector<parse_element>;


struct parse_data
{
	size_t token_count;
	size_t value_count;
	size_t literal_count;
	tokenlist tokens;
	tokenlist values;
	tokenlist literals;
	parse_element_list elements;

	parse_data() :token_count(0), value_count(0), literal_count(0) {}
};

result_type universal_parse(string_iter start, string_iter end, parse_data&p_data);


void store_element(parse_data&p_data, parse_element&element);