#include "stdafx.h"
#include "UParser.h"

result_type universal_parse(string_iter start, string_iter end, parse_data&p_data)
{
	enum class parse_state
	{
		scanning,
		structure,
		token,
		store,
		value,
		brackets,
		literal,
	};

	auto sIter = start;


	parse_state ustate = parse_state::scanning;

	u32 bracketcount = 0;

	parse_element current_element;

#define RESET_ELEMENT() do { 						   \
 		current_element.index = 0;					   \
		current_element.start = sIter;				   \
		current_element.end = sIter;				   \
		current_element.type = parse_type::structure;  \
	} while(0);

	RESET_ELEMENT();

	char current_quote = 0;
	bool has_escape = false;

	while (sIter != end)
	{
		switch (ustate)
		{
		case parse_state::scanning:
		{
			if (is_alpha(*sIter)) {
				current_element.type = parse_type::token;
				current_element.start = sIter;

				ustate = parse_state::token;
				continue;
			}

			if (is_numeric(*sIter)) {
				current_element.type = parse_type::value;
				current_element.start = sIter;

				ustate = parse_state::value;
				continue;
			}

			if (is_quote(*sIter))
			{
				current_quote = *sIter;
				current_element.type = parse_type::literal;
				current_element.start = sIter;

				ustate = parse_state::literal;
				break;
			}

			current_element.type = parse_type::structure;
			current_element.start = sIter;
			ustate = parse_state::structure;
			continue;

		} break;
		case parse_state::token:
		{
			if (!is_alpha_or_numeric(*sIter))
			{
				ustate = parse_state::store;
				continue;
			}
		} break;
		case parse_state::value:
		{
			if (!is_numeric_or_decimal(*sIter))
			{
				ustate = parse_state::store;
				continue;
			}
		} break;
		case parse_state::literal:
		{
			if (*sIter == current_quote)
			{
				if (!has_escape)
				{
					ustate = parse_state::store;
				}
			}
			if (*sIter == '\\') {
				if (has_escape)
					has_escape = false;
				else
					has_escape = true;
			}
			else {
				has_escape = false;
			}

		} break;
		case parse_state::structure:
		{
			if (is_alpha_or_numeric_or_quotes(*sIter))
			{
				ustate = parse_state::store;
				continue;
			}
		} break;
		case parse_state::store:
		{
			current_element.end = sIter;
			store_element(p_data, current_element);
			RESET_ELEMENT();
			ustate = parse_state::scanning;
		} continue;
		default:
			break;
		}
	
		sIter++;
	}
	current_element.end = end;

	if (current_element.start < current_element.end)
	{
		current_element.end = sIter;
		store_element(p_data, current_element);
	}



	return result_type();
}

void store_element(parse_data & p_data, parse_element & element)
{
	
	switch (element.type)
	{
	case parse_type::structure:
		break;
	case parse_type::token:
		element.index = p_data.token_count++;
		p_data.tokens[element.index] = std::string(element.start, element.end);
		break;
	case parse_type::value:
		element.index = p_data.value_count++;
		p_data.values[element.index] = std::string(element.start, element.end);
		break;
	case parse_type::literal:
		element.index = p_data.literal_count++;
		p_data.literals[element.index] = std::string(element.start, element.end);
		break;

	default:
		element.index = 0;
		break;
	}
	p_data.elements.push_back(element);
}
