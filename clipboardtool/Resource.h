//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by clipboardtool.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CLIPBOARDTOOL_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_LIST1                       1000
#define IDC_REFRESH                     1001
#define IDC_CURRENTLINE                 1002
#define IDC_PROCESS                     1003
#define IDC_COLUMN_LIST                 1004
#define IDC_DELETE_COLUMN               1005
#define IDC_MOVE_COLUMN                 1006
#define IDC_DUPLICATE_COLUMN            1007
#define IDC_TOGGLE_QUOTES               1008
#define IDC_Brackets                    1009
#define IDC_CROP                        1010
#define IDC_CROPPED                     1010
#define IDC_PARSE_BUTTON                1011
#define IDC_PARSE_DATA                  1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
