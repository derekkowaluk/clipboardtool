
// ClipTestDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include <string>
#include <vector>
#include <memory>


// CClipTestDlg dialog
class CClipTestDlg : public CDialogEx
{
// Construction
public:
	CClipTestDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_CLIPTEST_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;
	std::vector<std::wstring>	history_m;
	std::string					processed_m;
	std::vector<std::vector<std::string>> current_m;
	bool						brackets_m;
	bool						showCropped_m;
	HWND hwndNextViewer;

	std::unique_ptr<CFont>		listfont_m;

	void updateAndShowProcessed();
	void updateHistory();
	void refreshClipboard();

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRefresh();
	CListBox ClipList;
	afx_msg void OnClipboardUpdate();
	afx_msg void OnChangeCbChain(HWND hWndRemove, HWND hWndAfter);
	afx_msg void OnDestroy();
	afx_msg void OnDrawClipboard();
	afx_msg void OnLbnSelchangeList1();
	CEdit currentSelectionText_m;
	afx_msg void OnBnClickedProcess();
	CComboBox column_list;
	afx_msg void OnBnClickedDeleteColumn();
	afx_msg void OnBnClickedMoveColumn();
	afx_msg void OnBnClickedToggleQuotes();
	afx_msg void OnBnClickedDuplicateColumn();
	afx_msg void OnBnClickedBrackets();
	afx_msg void OnBnClickedCropped();
	afx_msg void OnGetMinMaxInfo(MINMAXINFO* lpMMI);
	afx_msg void OnBnClickedParseData();
};
