
// ClipTestDlg.cpp : implementation file
//

#include "stdafx.h"
#include "clipboardtool.h"
#include "maindialog.h"
#include "afxdialogex.h"
#include <DKMisc.h>
#include <DKText.h>
#include <sstream>
#include "UParser.h"

#pragma comment( lib, "d2d1.lib")


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnClipboardUpdate();
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
	ON_WM_CLIPBOARDUPDATE()
END_MESSAGE_MAP()


// CClipTestDlg dialog



CClipTestDlg::CClipTestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_CLIPBOARDTOOL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClipTestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, ClipList);
	DDX_Control(pDX, IDC_CURRENTLINE, currentSelectionText_m);
	DDX_Control(pDX, IDC_COLUMN_LIST, column_list);
}

BEGIN_MESSAGE_MAP(CClipTestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_REFRESH, &CClipTestDlg::OnBnClickedRefresh)
	ON_WM_CLIPBOARDUPDATE()
	ON_WM_CHANGECBCHAIN()
	ON_WM_DESTROY()
	ON_WM_DRAWCLIPBOARD()
	ON_LBN_SELCHANGE(IDC_LIST1, &CClipTestDlg::OnLbnSelchangeList1)
	ON_BN_CLICKED(IDC_PROCESS, &CClipTestDlg::OnBnClickedProcess)
	ON_BN_CLICKED(IDC_DELETE_COLUMN, &CClipTestDlg::OnBnClickedDeleteColumn)
	ON_BN_CLICKED(IDC_MOVE_COLUMN, &CClipTestDlg::OnBnClickedMoveColumn)
	ON_BN_CLICKED(IDC_TOGGLE_QUOTES, &CClipTestDlg::OnBnClickedToggleQuotes)
	ON_BN_CLICKED(IDC_DUPLICATE_COLUMN, &CClipTestDlg::OnBnClickedDuplicateColumn)
	ON_BN_CLICKED(IDC_Brackets, &CClipTestDlg::OnBnClickedBrackets)
	ON_BN_CLICKED(IDC_CROPPED, &CClipTestDlg::OnBnClickedCropped)
	ON_WM_GETMINMAXINFO()
	ON_BN_CLICKED(IDC_PARSE_DATA, &CClipTestDlg::OnBnClickedParseData)
END_MESSAGE_MAP()


// CClipTestDlg message handlers

void CClipTestDlg::updateAndShowProcessed()
{

	using namespace std::string_literals;
	using namespace DKText;

	column_list.ResetContent();

	size_t colcount = 0;
	for (auto&eachcolumn : current_m.front())
	{
		column_list.AddString(std::wstring(1, static_cast<wchar_t>(colcount + L'0')).c_str());
		colcount++;
	}
	decltype(current_m)::value_type tempLine;

	decltype(current_m)::value_type * working;
	std::stringstream ss;

	constexpr auto ssize = 8;

	for (auto&eachline : current_m)
	{
		working = &eachline;

		if (showCropped_m)
		{
			tempLine.clear();
			for (auto eachstring : eachline)
			{
				if (eachstring.size() > ssize)
				{
					eachstring.resize(ssize);
					eachstring.back() = '~';
				}
				else {
					eachstring.resize(ssize, ' ');
				}

				tempLine.emplace_back(std::move(eachstring));
			}
			working = &tempLine;
		}

		
		if (brackets_m)
			ss << "{ " << DKText::joinWithString(*working, ",\t"s) << " },\r\n"s;
		else
			ss << DKText::joinWithString(*working, "\t"s) << "\r\n"s;
	}
	processed_m = ss.str();


	//processed_m = DKText::joinWithString(stringLines, "\r\n"s);


	currentSelectionText_m.SetWindowTextW(to_wstring(processed_m).c_str());
	int tabsize = 24;
	currentSelectionText_m.SetTabStops(tabsize);
}

void CClipTestDlg::updateHistory()
{
	if (!IsClipboardFormatAvailable(CF_TEXT))
		return;
	
	int clipboardTries = 5;
	// clipboard might not open on first try...
	while (!OpenClipboard())
	{
		Sleep(10);
		if (!clipboardTries--) return;
	}
	{
		auto textHandle = GetClipboardData(CF_TEXT);
		if (textHandle != NULL)
		{

			auto lptstr = GlobalLock(textHandle);
			if (lptstr != NULL)
			{
				auto str = std::string(static_cast<const char *>(lptstr));
				auto wstr = std::wstring(str.begin(), str.end());
				if (history_m.size())
				{
					if (history_m.back() != wstr) history_m.push_back(wstr);
				} else history_m.push_back(wstr);
				GlobalUnlock(lptstr);
			}
		}
		CloseClipboard();
	}
}

void CClipTestDlg::refreshClipboard()
{
	constexpr auto MAXSTRSIZE = 50;
	updateHistory();
	ClipList.ResetContent();

	for (auto&eachone : history_m)
	{

		if (eachone.size() < MAXSTRSIZE)
			ClipList.AddString(eachone.c_str());
		else {
			auto smaller = eachone.substr(0, MAXSTRSIZE - 3) + std::wstring(L"...");
			ClipList.AddString(smaller.c_str());
		}
	}
}

BOOL CClipTestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	hwndNextViewer = SetClipboardViewer();
	showCropped_m = false;

	if (!listfont_m)
	{
		listfont_m = std::make_unique<CFont>();
		listfont_m->CreateFont(20, 0, 0, 0, FW_DONTCARE, false, false, 0, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FIXED_PITCH | FF_MODERN, _T("Courier New"));
	}

	currentSelectionText_m.SetFont(listfont_m.get());

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CClipTestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClipTestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClipTestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CClipTestDlg::OnBnClickedRefresh()
{
	// TODO: Add your control notification handler code here
	refreshClipboard();

}


void CAboutDlg::OnClipboardUpdate()
{
	// This feature requires Windows Vista or greater.
	// The symbol _WIN32_WINNT must be >= 0x0600.
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnClipboardUpdate();
}


void CClipTestDlg::OnClipboardUpdate()
{
	// This feature requires Windows Vista or greater.
	// The symbol _WIN32_WINNT must be >= 0x0600.
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnClipboardUpdate();
	refreshClipboard();
}


void CClipTestDlg::OnChangeCbChain(HWND hWndRemove, HWND hWndAfter)
{

	if (hWndRemove == hwndNextViewer)
		hwndNextViewer = hWndAfter;

	// Otherwise, pass the message to the next link. 

	else if (hwndNextViewer != NULL)
		::SendMessage(hwndNextViewer, WM_CHANGECBCHAIN, (WPARAM)hWndRemove, (LPARAM)hWndAfter);

	// TODO: Add your message handler code here
}


void CClipTestDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	ChangeClipboardChain(hwndNextViewer);
	// TODO: Add your message handler code here
}


void CClipTestDlg::OnDrawClipboard()
{
	CDialogEx::OnDrawClipboard();
	refreshClipboard();
	// TODO: Add your message handler code here
}


void CClipTestDlg::OnLbnSelchangeList1()
{
	// TODO: Add your control notification handler code here
	int idx = ClipList.GetCurSel();
	if (idx == -1) return;
	auto&selected = history_m[idx];
	currentSelectionText_m.SetWindowTextW(selected.c_str());

}


void CClipTestDlg::OnBnClickedProcess()
{
	using namespace std::string_literals;
	using namespace DKText;

	brackets_m = false;


	// TODO: Add your control notification handler code here
	int idx = ClipList.GetCurSel();
	if (idx == -1) return;
	auto&selected = history_m[idx];
	
	auto stringLines = DKText::splitAndTrim(to_string(selected), "\r\n"s);
	
	stringLines.erase(std::remove(stringLines.begin(), stringLines.end(), ""), stringLines.end());
	current_m.clear();
	
	std::string comment;
	for (auto&eachline : stringLines)
	{

		if (eachline.find_first_of("//") == std::string::npos)
		{
			comment = "";
		} else{
			comment = eachline;
			DKText::removeBeforePattern(comment, "//"s);
			DKText::trimOutsides(comment);
		}



		DKText::removeBeforePattern(eachline, "{"s);
		//DKMisc::removeAfterPattern(eachline, "//"s);
		DKText::removeAfterPattern(eachline, "}"s);
		DKText::rightTrim(eachline);
		current_m.emplace_back(DKText::splitAndTrim(eachline, ","s));
		current_m.back().push_back(comment);
		/*auto stringlist = DKText::splitAndTrim(eachline, ","s);*/
		eachline = DKText::joinWithString(current_m.back(), "\t"s);
	}



	updateAndShowProcessed();

	column_list.SetCurSel(0);


}



void CClipTestDlg::OnBnClickedDeleteColumn()
{
	// TODO: Add your control notification handler code here
	using namespace std::string_literals;
	using namespace DKText;

	int colnumber = column_list.GetCurSel();

	if (colnumber >= 0)
	{
		for (auto&eachline : current_m)
		{
			eachline.erase(eachline.begin() + colnumber);
		}
	}
	else return;

	updateAndShowProcessed();
	column_list.SetCurSel(0);
}


void CClipTestDlg::OnBnClickedMoveColumn()
{
	// TODO: Add your control notification handler code here


	int colnumber = column_list.GetCurSel();
	int newpos;

	if (colnumber >= 0)
	{
		newpos = (colnumber + 1) % current_m.front().size();

		for (auto&eachline : current_m)
		{
			auto item = eachline[colnumber];

			eachline.erase(eachline.begin() + colnumber);
			eachline.insert(eachline.begin() + newpos, item);
		}
	}
	else return;

	updateAndShowProcessed();
	column_list.SetCurSel(newpos);
}


void CClipTestDlg::OnBnClickedToggleQuotes()
{
	int colnumber = column_list.GetCurSel();

	if (colnumber >= 0)
	{

		for (auto&eachline : current_m)
		{
			if (eachline.size() <= colnumber) continue;
			if (eachline[colnumber].size() == 0) continue;
		
			auto&item = eachline[colnumber];
			if (item.front() == '"')
				if (item.back() == '"')
				{
					item.pop_back();
					item.erase(item.begin());
					continue;
				}
			item.insert(item.begin(), '"');
			item.push_back('"');

		}
	}
	else return;

	updateAndShowProcessed();
	column_list.SetCurSel(colnumber);
}


void CClipTestDlg::OnBnClickedDuplicateColumn()
{
	// TODO: Add your control notification handler code here
	int colnumber = column_list.GetCurSel();
	int newpos;

	if (colnumber >= 0)
	{
		newpos = (colnumber + 1);

		for (auto&eachline : current_m)
		{
			auto item = eachline[colnumber];

			eachline.insert(eachline.begin() + newpos, item);
		}
	}
	else return;

	updateAndShowProcessed();
	column_list.SetCurSel(newpos);
}


void CClipTestDlg::OnBnClickedBrackets()
{
	// TODO: Add your control notification handler code here
	int colnumber = column_list.GetCurSel();
	brackets_m = !brackets_m;
	updateAndShowProcessed();
	column_list.SetCurSel(colnumber);
}


void CClipTestDlg::OnBnClickedCropped()
{
	// TODO: Add your control notification handler code here
	showCropped_m = !showCropped_m;
	int colnumber = column_list.GetCurSel();
	updateAndShowProcessed();
	column_list.SetCurSel(colnumber);
}


void CClipTestDlg::OnGetMinMaxInfo(MINMAXINFO* lpMMI)
{
	// TODO: Add your message handler code here and/or call default

	CDialogEx::OnGetMinMaxInfo(lpMMI);

	FLOAT dpiX, dpiY;
	{
		ID2D1Factory* m_pDirect2dFactory;
		D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &m_pDirect2dFactory);
		m_pDirect2dFactory->GetDesktopDpi(&dpiX, &dpiY);
		m_pDirect2dFactory->Release();
	}

	
	lpMMI->ptMinTrackSize.x = (700 * dpiX)/96;
	lpMMI->ptMinTrackSize.y = (450 * dpiX)/96;
	
}


void CClipTestDlg::OnBnClickedParseData()
{
	int idx = ClipList.GetCurSel();
	if (idx == -1) return;
	auto&selected = history_m[idx];


	std::string p_string(selected.begin(), selected.end());
	
	std::wstringstream ss;

	parse_data p_data;

	universal_parse(p_string.cbegin(), p_string.cend(), p_data);

	for (auto&each_element : p_data.elements)
	{
		auto type = each_element.type;

		switch (type)
		{
		case parse_type::structure:
			ss << std::wstring(each_element.start, each_element.end);
			break;
		case parse_type::token:
			ss << "#T(" << each_element.index << ")";
			break;
		case parse_type::value:
			ss << "#V(" << each_element.index << ")";
			break;
		case parse_type::literal:
			ss << "#L(" << each_element.index << ")";
		default:
			break;
		}
	}


	currentSelectionText_m.SetWindowTextW(ss.str().c_str());

}
