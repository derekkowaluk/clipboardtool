#pragma once
/*
* File:   common_types.h
* Author: Derek Kowaluk
*
* Created on February 21, 2018, 9:47 AM
*/

#ifndef COMMON_TYPES_H
#define	COMMON_TYPES_H

#include <stdint.h>
#include <stdbool.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;

//typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;


typedef u8 * pU8;
typedef u16 * pU16;
typedef u32 * pU32;

#define pcast_u8(value) ((uint8_t *)(value))
#define cast_u8(value)  ((uint8_t)(value))
#define cast_u32(value) ((uint32_t)(value))
#define cast_u16(value) ((uint16_t)(value))


#ifdef	__cplusplus

#define PACKSTRUCT
extern "C" {

#else

#define nullptr 0
#define PACKSTRUCT __attribute__ ((packed)) 

#endif


#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* COMMON_TYPES_H */